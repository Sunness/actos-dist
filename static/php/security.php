<?php

require 'ajax_check.php';
session_start();
include 'init.php';

$data = array('success' => false);
$post = json_decode(file_get_contents('php://input'), true);
if (isset($post['checksum']) && $post['checksum']) {
    $data['checksum'] = getChecksum();
    $data['success'] = true;
}

if (isset($post['user_name']) && $post['user_name']) {
    $data['user_name'] = getUserName();
    if (isAdmin()) {
        $data['admin'] = '<a href="admin.html">Admin</a>';
    }
    $data['success'] = true;
}
echo json_encode($data);
