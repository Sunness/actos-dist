<?php

require 'ajax_check.php';
if (!session_id()) {
    session_start();
}
include 'init.php';
$data = array('success' => false, 'request_history' => array(), 'error'=>'Unknown Error');
$post = json_decode(file_get_contents('php://input'), true);

if (isset($post['checksum']) && $_SESSION['checksum'] === $post['checksum']) {
    require 'base.php';

    if ($ini['DEBUG']) {
        $data['debug']['POST'] = $post;
        $data['debug']['SESSION'] = $_SESSION;
    }

    $id = $_SESSION['user_session'];
    $stmt = $conn->prepare('SELECT TimeOff.ID AS id,
                                   RequestDate AS request_date,
                                   OffStartDate AS off_start_date,
                                   OffEndDate AS off_end_date,
                                   Duration AS duration,
                                   OffType.Name AS type,
                                   StatusCode.Name AS status
                            FROM TimeOff
                            JOIN OffType ON OffType.ID = TypeID
                            JOIN StatusCode ON StatusCode.ID = Status
                            WHERE EmpID = ? AND Status < 103
                            ORDER BY request_date, off_start_date, TypeID, Status');
    if (!$stmt) {
        $data['error'] = $conn->error;
        die(json_encode($data));
    }
    $stmt->bind_param('i', $id);
    if ($stmt->execute() &&
        $stmt->store_result() &&
        $stmt->bind_result($id, $request_date, $off_start_date, $off_end_date, $duration, $reason, $status) &&
        $stmt->num_rows > 0) {
        while ($stmt->fetch()) {
            $data['request_history'][] = array(
              'id' => $id,
              'request_date' => $request_date,
              'off_start_date' => $off_start_date,
              'off_end_date' => $off_end_date,
              'duration' => $duration,
              'reason' => $reason,
              'status' => $status,
              'loading' => false,
            );
        }
    }
    if ($ini['DEBUG']) {
        $data['debug']['STMT'] = $stmt;
        $data['debug']['CONN'] = $conn;
    }
    $data['success'] = true;
    isset($stmt) && $stmt->close();
    $conn->close();
} else {
    $data['error'] = 'Invalid POST data';
}
$data['checksum'] = getChecksum();
echo json_encode($data);
