<?php

require 'ajax_check.php';
include 'init.php';
require 'base.php';
if ($ini['DEBUG']) {
    sleep(1);
}
$conn->autocommit(false);
session_start();
$data = array('success' => false, 'error'=>'Unknown Error');
$post = json_decode(file_get_contents('php://input'), true);
$TYPEID = array('misc' => 1000, 'sick' => 1001, 'vac' => 1002);
$ini['DEBUG'] && $data['debug']['POST'] = $post;
$ini['DEBUG'] && $data['debug']['SESSION'] = $_SESSION;
if (isset($post['checksum']) && $_SESSION['checksum'] === $post['checksum']) {
    $request_date = isset($post['request_date']) ?
                        empty(trim($post['request_date'])) ? null : trim($post['request_date']) : null;
    $off_start_date = isset($post['off_start_date']) ?
                        empty(trim($post['off_start_date'])) ? null : trim($post['off_start_date']) : null;
    $off_end_date = isset($post['off_end_date']) ?
                        empty(trim($post['off_end_date'])) ? null : trim($post['off_end_date']) : null;
    $duration = isset($post['duration']) ?
                        empty(trim($post['duration'])) ? null : trim($post['duration']) : null;
    $reason = isset($post['reason']) ?
                        empty(trim($post['reason'])) ? null : $TYPEID[trim($post['reason'])] : null;
    $request_date = date('Y-m-d H:i:s', strtotime($request_date));
    if ($ini['DEBUG']) {
        $data['debug']['request_date'] = $request_date;
        $data['debug']['off_start_date'] = $off_start_date;
        $data['debug']['off_end_date'] = $off_end_date;
        $data['debug']['duration'] = $duration;
        $data['debug']['reason'] = $reason;
    }
    try {
        if ($request_date && $off_start_date && $off_end_date && $duration && $reason) {
            $id = $_SESSION['user_session'];
            $ini['DEBUG'] && $data['debug']['id'] = $id;
            $stmt = $conn->prepare('INSERT INTO TimeOff (EmpID, RequestDate, OffStartDate, OffEndDate, Duration, TypeID)
                                          VALUES  (?, ?, ?, ?, ?, ?)');
            if (!$stmt) {
                $data['error'] = $conn->error;
                die(json_encode($data));
            }
            $stmt->bind_param('issssi', $id, $request_date, $off_start_date, $off_end_date, $duration, $reason);
            $stmt->execute();
            $conn->commit();
            $data['success'] = true;
        }
    } catch (mysqli_sql_exception $e) {
        $conn->rollback();
        $data['error'] = $e->getMessage();
        die(json_encode($data));
    } finally {
        isset($stmt) && $stmt->close();
        $conn->autocommit(true);
        $conn->close();
    }
} else {
    $data['error'] = 'Invalid POST data';
}
$data['checksum'] = getChecksum();
echo json_encode($data);
