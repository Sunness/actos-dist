<?php
$ini = parse_ini_file('settings.ini', true);
$STATUS = array('Pending' => '<span class="visible-xs-inline-block glyphicon glyphicon-refresh rotate"></span>',
                'Paid' => '<span class="visible-xs-inline-block glyphicon glyphicon-ok"></span>',
                'Not paid' => '<span class="visible-xs-inline-block glyphicon glyphicon-remove"></span>', );
$TYPE = array('Miscellaneous' => '<span class="visible-xs-inline-block glyphicon glyphicon-home"></span>',
              'Sick' => '<span class="visible-xs-inline-block glyphicon glyphicon-bed"></span>',
              'Vacation' => '<span class="visible-xs-inline-block glyphicon glyphicon-plane"></span>', );
$conn = new mysqli($ini['Database']['Address'], $ini['User']['Username'], $ini['User']['Password'], $ini['Database']['Database']);
if ($conn->connect_error) {
    die('Connect failed: '.$conn->connect_error);
}
