<?php

function convertDate($date, $return)
{
    return is_null($date) ? 0 : (strtotime($date) ? $return : 0);
}

function convertBooleanArray($array)
{
    $return = 0;
    if (is_array($array)) {
        foreach ($array as $key => $value) {
            $return <<= 1;
            if ($value === 'true') {
                $return ^= 1;
            }
        }
    }

    return $return;
}

function convertString($string)
{
    return is_null($string) ? 0 : (empty(trim($string)) ? 0 : (is_numeric($string) ? 1 : 2));
}

function startsWith($haystack, $needle)
{
    $length = strlen($needle);

    return substr($haystack, 0, $length) === $needle;
}

function endsWith($haystack, $needle)
{
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return substr($haystack, -strlen($needle)) === $needle;
}

function checkSqlTail($sql)
{
    $sql = trim($sql);
    $tails = array(' AND () AND', ' AND()', ' AND');
    $length = strlen($sql);
    $taillen = -1;
    foreach ($tails as $tail) {
        if (endsWith($sql, $tail)) {
            $taillen = strlen($tail);
        }
    }

    return $taillen > 0 ? substr($sql, 0, $length - $taillen) : $sql;
}
