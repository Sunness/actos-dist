<?php
require 'ajax_check.php';
session_start();
include 'init.php';

$data = array('success' => false, 'error'=>'Unknown Error');
$post = json_decode(file_get_contents('php://input'), true);
if (isset($post['checksum']) && $_SESSION['checksum'] === $post['checksum'] &&
    isset($_SESSION['admin']) && $_SESSION['admin'] === 1) {
    require 'base.php';
    $conn->close();
    $conn = new mysqli($ini['Database']['Address'], $ini['Admin']['Username'], $ini['Admin']['Password'], $ini['Database']['Database']);

    if ($ini['DEBUG']) {
        sleep(1);
        $data['debug'] = array('POST' => $post);
    }

    $off_id = isset($post['id']) ? is_numeric(trim($post['id'])) ? trim($post['id']) : null : null;
    $status = isset($post['status']) ? empty(trim($post['status'])) ? null : trim($post['status']) : null;
    $ini['DEBUG'] && $data['debug']['id'] = $off_id;
    $ini['DEBUG'] && $data['debug']['status'] = $status;
    try {
        if ($off_id && $status) {
            $stmt = $conn->prepare('UPDATE TimeOff dest,
                                    (
                                      SELECT ID
                                      FROM StatusCode
                                      WHERE Name = ?
                                    ) src
                                    SET dest.Status = src.ID WHERE dest.ID = ?');
            if (!$stmt) {
                $data['error'] = $conn->error;
                die(json_encode($data));
            }
            $stmt->bind_param('si', $status, $off_id);
            $stmt->execute();
            $conn->commit();
            $data['success'] = true;
        }
    } catch (mysqli_sql_exception $e) {
        $conn->rollback();
        $data['error'] = $e->getMessage();
        die(json_encode($data));
    } finally {
        isset($stmt) && $stmt->close();
        $conn->autocommit(true);
        $conn->close();
    }
}
$data['checksum'] = getChecksum();
echo json_encode($data);
