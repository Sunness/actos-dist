<?php

require 'ajax_check.php';
session_start();

include 'init.php';
$data = array('success' => false, 'application_history' => array(), 'error' => 'Unknown Error');
$post = json_decode(file_get_contents('php://input'), true);

if (isset($post['checksum']) && $_SESSION['checksum'] === $post['checksum'] &&
    isset($_SESSION['admin']) && $_SESSION['admin'] === 1) {
    require 'base.php';

    if ($ini['DEBUG']) {
        sleep(1);
        $data['debug'] = array('POST' => $post);
    }

    if (!isset($_SESSION['last_access']) || (time() - $_SESSION['last_access']) > 60) {
        $_SESSION['last_access'] = time();
    }
    try {
        $stmt = $conn->prepare('SELECT tf.ID AS off_id,
                                       tf.EmpID AS emp_id,
                                       e.Name AS name,
                                       tf.RequestDate AS request_date,
                                       tf.OffStartDate AS off_start_date,
                                       tf.OffEndDate AS off_end_date,
                                       tf.Duration AS duration,
                                       ot.Name AS reason,
                                       sc.Name AS status
                                    FROM TimeOff AS tf
                                    JOIN OffType AS ot ON ot.ID = tf.TypeID
                                    JOIN StatusCode AS sc ON sc.ID = tf.Status
                                    JOIN Employee AS e ON e.ID = tf.EmpID
                                    WHERE tf.Status < 103
                                    ORDER BY tf.RequestDate, tf.OffStartDate, tf.TypeID, tf.Status');
        if (!$stmt) {
            $data['error'] = $conn->error;
            die(json_encode($data));
        }
        if ($stmt->execute() &&
            $stmt->store_result() &&
            $stmt->bind_result($id, $empid, $name, $request_date, $off_start_date, $off_end_date, $duration, $reason, $status)) {
            while ($stmt->fetch()) {
                $data['application'][] = array(
                  'id' => $id,
                  'empid' => $empid,
                  'name' => $name,
                  'request_date' => $request_date,
                  'off_start_date' => $off_start_date,
                  'off_end_date' => $off_end_date,
                  'duration' => $duration,
                  'reason' => $reason,
                  'status' => $status,
                  'loading' => array('paid' => false, 'notpaid' => false, 'del' => false),
                  'edit' => $status === 'Pending', );
            }
            $data['success'] = true;
        } else {
            $data['error'] = $stmt->error.', '.$conn->error;
        }
    } catch (mysqli_sql_exception $e) {
        $data['error'] = $e->geMessage();
        die(json_encode($data));
    } finally {
        isset($stmt) && $stmt->close();
        $conn->close();
    }
}
$data['checksum'] = getChecksum();
echo json_encode($data);
