<?php

require 'ajax_check.php';
require 'base.php';
include 'init.php';

if (!session_id()) {
    session_start();
}
$data = array('success' => false, 'error' => 'Unknown Error');

$post = json_decode(file_get_contents('php://input'), true);
if ($ini['DEBUG']) {
    $data['debug']['POST'] = $post;
    $data['debug']['SESSION'] = $_SESSION;
}
if (isset($post['checksum']) && ($post['checksum'] === $_SESSION['checksum'])
&& isset($post['empid']) && isset($post['pwd']) && !empty($post['empid']) && !empty($post['pwd'])) {
    $stmt = $conn->prepare('select Password, Admin, Name from User join Employee on EmpID=ID where EmpID = ?');
    if (!$stmt) {
        $data['error'] = $conn->error;
        die(json_encode($data));
    }
    $id = trim($post['empid']);
    $pwd = trim($post['pwd']);
    $stmt->bind_param('i', $id);
    $stmt->execute();
    $stmt->bind_result($hpwd, $admin, $name);
    if (!$stmt->fetch()) {
        if (empty($stmt->error)) {
            $data['error'] = 'Employee ID and password does not match';
            die(json_encode($data));
        } else {
            $data['error'] = 'Error executing MySQL query: '.$stmt->error;
            die(json_encode($data));
        }
    }
    $stmt->close();
    $conn->close();
    if (password_verify($pwd, $hpwd)) {
        $_SESSION['admin'] = $admin;
        $_SESSION['user_session'] = $id;
        $_SESSION['user_name'] = $name;
        if (!isset($_SESSION['last_access']) || (time() - $_SESSION['last_access']) > 60) {
            $_SESSION['last_access'] = time();
        }
        $data['success'] = true;
        $data['user_name'] = $name;
        $data['admin'] = $admin === 1;
    } else {
        $data['error'] = 'Employee ID and password does not match';
        die(json_encode($data));
    }
} else {
    $data['error'] = 'Invalid data';
    $ini['DEBUG'] && $data['debug']['POST'] = $post;
    die(json_encode($data));
}
$data['checksum'] = getChecksum();
echo json_encode($data);
