<?php

function getChecksum()
{
    $value = dechex(crc32(md5(sha1(rand()).'Amberson').'College'));
    $_SESSION['checksum'] = $value;

    return $value;
}

function getUserName()
{
    return $_SESSION['user_name'];
}

function isAdmin()
{
    return isset($_SESSION['admin']) ? $_SESSION['admin'] : false;
}
