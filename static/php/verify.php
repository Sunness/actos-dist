<?php
require 'ajax_check.php';
session_start();
$data = array('verified' => false);
if (!isset($_SESSION['user_name']) || empty($_SESSION['user_name']) ||
!isset($_SESSION['user_session']) || empty($_SESSION['user_session'])) {
    header('HTTP/1.0 401 Unauthorized');
    die('Unauthorized Action');
} else {
    require 'base.php';
    $ini['DEBUG'] && $data['debug']['POST'] = $_POST;
    $ini['DEBUG'] && $data['debug']['SESSION'] = $_SESSION;
    $id = $_SESSION['user_session'];
    if (isset($_POST['admin']) && $_POST['admin']) {
        $stmt = $conn->prepare('select Admin, Name from User join Employee on EmpID = ID where ID = ?');
        if (!$stmt) {
            $data['error'] = $conn->error;
            die(json_encode($data));
        }
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->bind_result($admin, $name);
        if (!$stmt->fetch()) {
            if (empty($stmt->error)) {
                header('HTTP/1.0 401 Unauthorized');
                die('Unauthorized Action');
            } else {
                header('HTTP/1.0 500 Internal Server Error');
                die('Unauthorized Action');
            }
        }
        $stmt->close();
        $conn->close();
        $ini['DEBUG'] && $data['debug']['Name'] = $name;
        $ini['DEBUG'] && $data['debug']['Admin'] = $admin;
        if (($name !== $_SESSION['user_name']) || ($admin !== $_SESSION['admin']) || $admin === 0) {
            header('HTTP/1.0 401 Unauthorized');
            die('Unauthorized Action');
        }
    } else {
        $stmt = $conn->prepare('select Name from Employee where ID = ?');
        if (!$stmt) {
            $data['error'] = $conn->error;
            die(json_encode($data));
        }
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->bind_result($name);
        if (!$stmt->fetch()) {
            if (empty($stmt->error)) {
                header('HTTP/1.0 401 Unauthorized');
                die('Unauthorized Action');
            } else {
                header('HTTP/1.0 500 Internal Server Error');
                die('Unauthorized Action');
            }
        }
        $stmt->close();
        $conn->close();
        if ($name !== $_SESSION['user_name']) {
            header('HTTP/1.0 401 Unauthorized');
            die('Unauthorized Action');
        }
    }

    $data['verified'] = true;
    echo json_encode($data);
}
