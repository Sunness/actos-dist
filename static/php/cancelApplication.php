<?php

require 'ajax_check.php';
if (!session_id()) {
    session_start();
}
include 'init.php';
$data = array('success' => false, 'error'=>'Unknown Error');
$post = json_decode(file_get_contents('php://input'), true);

if (isset($post['checksum']) && $_SESSION['checksum'] === $post['checksum']) {
    require 'base.php';
    $conn->close();
    $conn = new mysqli($ini['Database']['Address'], $ini['Admin']['Username'], $ini['Admin']['Password'], $ini['Database']['Database']);
    if ($conn->connect_error) {
        $data['error'] = $conn->connext_error;
        die(json_encode($data));
    }

    if ($ini['DEBUG']) {
        $data['debug']['POST'] = $post;
    }

    if (isset($post['id']) && !empty(trim($post['id'])) && is_numeric(trim($post['id']))) {
        $empid = $_SESSION['user_session'];
        $id = trim($post['id']);
        $conn->autocommit(false);
        try {
            $stmt = $conn->prepare('UPDATE TimeOff SET Status = 103 WHERE ID = ? AND EmpID = ?');
            if (!$stmt) {
                $data['error'] = $conn->error;
                die(json_encode($data));
            }
            $stmt->bind_param('ii', $id, $empid);
            $stmt->execute();
            $conn->commit();
            $data['success'] = true;
        } catch (mysqli_sql_exception $e) {
            $conn->rollback();
            $data['error'] = $e->getMessage();
            die(json_encode($data));
        } finally {
            isset($stmt) && $stmt->close();
            $conn->autocommit(true);
            $conn->close();
        }
    } else {
        $data['error'] = 'Invalid POST data';
    }
} else {
    $data['error'] = 'Invalid POST data';
}
$data['checksum'] = getChecksum();
echo json_encode($data);
